@Library('EZJEL') _

def dockerImage
pipeline {
    agent {
        kubernetes {
        label 'ez-joy'
        idleMinutes 5
        yamlFile 'build-pod.yaml'
        defaultContainer 'ez-docker-helm-build'
        }
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
    }

    environment {
        HELM_PACKAGE = 'ezezeasy/ez-joy-chart'
    }

    stages {
        stage('Setup') {
            steps {
                checkout scm
                script {
                    ezEnvSetup.initEnv()
                    def id = ezUtils.getUniqueBuildIdentifier()
                    if(BRANCH_NAME == 'main')
                    {
                        env.BUILD_ID = "1."+id
                    }
                    else {
                        env.BUILD_ID = "0." + ezUtils.getUniqueBuildIdentifier("issueNumber") + "." + id
                    }
                    currentBuild.displayName+=" {build-name:"+env.BUILD_ID+"}"
                }
            }
        }

        stage('Build Helm Chart') {
            steps {
                sh 'helm lint ez-joy-chart'
                sh 'helm package ez-joy-chart --version '+env.BUILD_ID
            }
        }

        stage('Push HELM chart') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'ez_dockerhub_credentials', passwordVariable: 'DOCKERHUB_PASSWORD', usernameVariable: 'DOCKERHUB_USER')]) {
                        sh "docker login -u ${DOCKERHUB_USER} -p ${DOCKERHUB_PASSWORD}"
                        sh 'helm push ez-joy-chart-'+env.BUILD_ID+'.tgz oci://registry-1.docker.io/ezezeasy'
                    }
                }
            }
        }
    }
}
